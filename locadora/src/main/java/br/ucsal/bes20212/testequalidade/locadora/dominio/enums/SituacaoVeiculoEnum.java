package br.ucsal.bes20212.testequalidade.locadora.dominio.enums;

public enum SituacaoVeiculoEnum {
	DISPONIVEL, MANUTENCAO, LOCADO;
}
